//  DLF_default.hpp
//  Membrane
//
//  Copyright © 2020 Daniele Liprandi. The Membrane project is under the CC by-nc-sa 4.0 license
//    Permission is granted to copy, distribute and/or modify this document according to the terms in
//    Creative Commons License, Attribution-NonCommercial-ShareAlike 4.0 International. The full text
//    of this license may be found here: CC by-nc-sa 4.0
//    (https://creativecommons.org/licenses/by-nc-sa/4.0/)
//

#ifndef DLF_DEFAULT_HPP_
#define DLF_DEFAULT_HPP_

#include <eigen3/Eigen/Eigen>
#include <stdio.h>
#include <sys/stat.h>

#include <cmath>
#include <iostream>
#include <string>
#include <tuple>
#include <vector>

using Eigen::Array;
using Eigen::Dynamic;
using Eigen::Matrix;

/* -------------------------------------------------------------------------- */
/*                        Typedefs for common 3D problem                      */
/* -------------------------------------------------------------------------- */
typedef Eigen::Index ind;

typedef Matrix<double, Dynamic, Dynamic> mat;
typedef Matrix<double, 3, Dynamic> mat3D;
typedef Eigen::Matrix3d mat3;
typedef Matrix<double, Dynamic, 1> vec;
typedef Eigen::Vector3d vec3;
typedef Eigen::Matrix<double, 6, 1> vec6;

typedef Array<double, 3, Dynamic> amat3D;
typedef Array<double, Dynamic, 1> avec;
typedef Eigen::Array3d avec3;

typedef Array<ind, Dynamic, Dynamic> indmat;
typedef Array<ind, 2, Dynamic> indmat2;
typedef Array<ind, Dynamic, 1> indvec;
typedef Array<bool, Dynamic, 1> boolvec;

typedef Eigen::Triplet<double> triplet;
typedef std::vector<triplet> tlist;

/* -------------------------------------------------------------------------- */
/*                         Template function for maths                        */
/* -------------------------------------------------------------------------- */

template <typename T> int sgn(T val) { return (T(0) < val) - (val < T(0)); }

template <typename T> T computeDifference(T a, T b) { return ((a * a - b * b) / (a + b)); }

template <typename T> double quadSum3(T a, T b, T c) { return (sqrt(a * a + b * b + c * c)); }
template <typename T> double quadSum2(T a, T b) { return (sqrt(a * a + b * b)); }
template <typename T> double module3dAtIndex(T a, int i) {
  return quadSum3(a(3 * i + 0), a(3 * i + 1), a(3 * i + 2));
}

/* -------------------------------------------------------------------------- */
/*                             Matrix manipulation                            */
/* -------------------------------------------------------------------------- */

template <typename eigen_mat> void removeColumn(eigen_mat &matrix, unsigned int colToRemove) {
  unsigned int numRows = matrix.rows();
  unsigned int numCols = matrix.cols() - 1;

  if (colToRemove < numCols)
    matrix.block(0, colToRemove, numRows, numCols - colToRemove) =
        matrix.block(0, colToRemove + 1, numRows, numCols - colToRemove);

  matrix.conservativeResize(numRows, numCols);
}

template <typename eigen_mat> eigen_mat buildMatFromIndvec(indvec const &uniques, eigen_mat const &m) {
  eigen_mat m_r = eigen_mat::Zero(m.rows(), uniques.nonZeros());
  int c = 0;
  for (int i = 0; i < uniques.nonZeros(); i++) {
    if (uniques(i) == 0) {
      m_r.col(c++) = m.col(i);
    }
  }
  return m_r;
}

template <typename eigen_mat> eigen_mat takeLeftColsBeforeZeros(eigen_mat const &m) {
  ind i = 0;
  for (; i < m.cols(); i++) {
    if (m(0, i) == 0 && m(1, i) == 0) {
      break;
    }
  }
  return m.leftCols(i);
}

//*----------------------------*
//|       File functions       |
//*----------------------------*

inline bool exists_file(const std::string &name) {
  struct stat buffer;
  return (stat(name.c_str(), &buffer) == 0);
}

#endif  // DLF_DEFAULT_HPP_
