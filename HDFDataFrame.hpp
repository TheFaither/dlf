/**
 * @file HDFDataFrame.hpp
 * @author Daniele Liprandi (daniele.liprandi@gmail.com)
 * @brief A file to manage huge sets of results by using Eigen and HDF5.
 * This entire file is made possible by this library: http://h5cpp.org
 * @date 2020-06-10
 *
 * @copyright Copyright © 2020. This file is under the CC by-nc-sa 4.0 license
 * Creative Commons License, Attribution-NonCommercial-ShareAlike 4.0 International.
 * The full text of this license may be found here:
 * https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 */

#ifndef DLF_HDFDATAFRAME_HPP_
#define DLF_HDFDATAFRAME_HPP_

#include <eigen3/Eigen/Dense>
#include <string>
#pragma GCC system_header
#include "h5cpp/all"

class HDFDataFrame {
 private:
  std::string filename;  ///< Name of the HDF5 file
  std::string output_folder;
  dlf::Timer HDFTimer;

 public:
  h5::fd_t fd;  ///< file
  HDFDataFrame();
  inline explicit HDFDataFrame(std::string filename, bool with_timestamp = false,
                               std::string folder = "out/", std::string inputfile = "") {
    output_folder = folder;
    this->filename = with_timestamp ? output_folder + filename + "_" + HDFTimer.timestamp()
                                    : output_folder + filename;
    if (inputfile != "") {
      const auto copyOptions = std::filesystem::copy_options::update_existing;
      std::filesystem::copy_file(inputfile, this->filename + ".ini", copyOptions);
    }
    createOver(this->filename);
  }

  inline std::string get_filename() const { return filename; }
  /**
   * @brief Create the file overwriting previous copies
   *
   * @param filename name of the HDF file
   */
  inline void createOver(std::string filename) {
    fd = h5::create(filename + ".hdf5", H5F_ACC_TRUNC);
  }
  void saveFile(); 
  void loadFile(std::string filename);
};

#endif  // DLF_HDFDATAFRAME_HPP_
