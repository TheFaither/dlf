/**
 * @file EigenAddons.hpp
 * @author Daniele Liprandi (daniele.liprandi@gmail.com)
 * @brief
 * @date 2020-06-04
 *
 * @copyright Copyright © 2020. This file is under the CC by-nc-sa 4.0 license
 * Creative Commons License, Attribution-NonCommercial-ShareAlike 4.0 International.
 * The full text of this license may be found here:
 * https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 */
// TODO(@TheFaither) read this https://eigen.tuxfamily.org/dox/TopicInsideEigenExample.html
//

#ifndef DLF_EIGENADDONS_HPP_
#define DLF_EIGENADDONS_HPP_

inline Scalar at(unsigned int i, unsigned int j) const { return this->operator()(i, j); }
inline Scalar &x(unsigned int i) { return this->operator()(3 * i + 0); }
inline Scalar &y(unsigned int i) { return this->operator()(3 * i + 1); }
inline Scalar &z(unsigned int i) { return this->operator()(3 * i + 2); }
inline Scalar x(unsigned int i) const { return this->operator()(3 * i + 0); }
inline Scalar y(unsigned int i) const { return this->operator()(3 * i + 1); }
inline Scalar z(unsigned int i) const { return this->operator()(3 * i + 2); }

inline long unsigned int usize() const { return static_cast<long unsigned int>(this->size()); }

inline Scalar module3D(unsigned int node) const {
  return sqrt(this->operator()(3 * node + 0) * this->operator()(3 * node + 0) +
              this->operator()(3 * node + 1) * this->operator()(3 * node + 1) +
              this->operator()(3 * node + 2) * this->operator()(3 * node + 2));
}

inline Scalar modulexy(unsigned int node) const {
  return sqrt(this->operator()(3 * node + 0) * this->operator()(3 * node + 0) +
              this->operator()(3 * node + 1) * this->operator()(3 * node + 1));
}

#endif  // DLF_EIGENADDONS_HPP_
