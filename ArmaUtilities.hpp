/**
 * @file ArmaUtilities.hpp
 * @author Daniele Liprandi (daniele.liprandi@gmail.com)
 * @brief
 * @date 2020-05-29
 *
 * @copyright Copyright © 2020. This file is under the CC by-nc-sa 4.0 license
 * Creative Commons License, Attribution-NonCommercial-ShareAlike 4.0 International.
 * The full text of this license may be found here:
 * https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 */

#ifndef DLF_ARMAUTILITIES_HPP_
#define DLF_ARMAUTILITIES_HPP_

#include <armadillo>

namespace armautils {

  using arma::uvec;

  template <typename T> inline bool approxEqual(const T &lhs, const T &rhs, double tol = 0.00000001) {
    return arma::approx_equal(lhs, rhs, "absdiff", tol);
  }

  inline arma::uvec obtainUvecUniqueCols(arma::Mat<Eigen::Index> const &m) {
    if (m.n_rows != 2) {
      std::cout << "Wrong number of rows. Should be 2, is: " << m.n_rows << std::endl;
    }
    arma::uvec ulmt = arma::zeros<arma::uvec>(m.n_cols);

    for (arma::uword i = 0; i < m.n_cols; i++) {
      for (arma::uword j = i + 1; j < m.n_cols; j++) {
        if (m.at(0, i) == m.at(0, j) && m.at(1, i) == m.at(1, j)) {
          ulmt(j) = 1;
          break;
        }
      }
    }
    return ulmt;
  }

  inline arma::superlu_opts defaultSuperLUSettings(arma::superlu_opts s) {
    s.permutation = arma::superlu_opts::COLAMD;
    s.refine = arma::superlu_opts::REF_NONE;
    s.symmetric = false;
    s.equilibrate = false;
    return s;
  }

  inline arma::mat deleteMinus(const arma::mat &m) {
    arma::mat m_c(m);
    arma::uword i = 0, c = 0;

    for (i = 0; c < m.n_rows; i++, c++) {
      if (m_c(i, 0) == -1.0 && m_c(i, 1) == -1.0 && m_c(i, 2) == -1.0) {
        try {
          m_c.shed_row(i);
        } catch (...) {
          std::cerr << "failed at " << c << std::endl;
        }
        i--;
      }
    }
    return m_c;
  }

  inline arma::umat unique_cols(const arma::umat &m) {
    arma::uvec vlmt = arma::zeros<arma::uvec>(m.n_cols);
    for (arma::uword i = 0; i < m.n_cols; i++) {
      for (arma::uword j = i + 1; j < m.n_cols; j++) {
        if (approxEqual(m.row(i), m.row(j))) {
          vlmt(j) = 1;
          break;
        }
      }
    }

    return m.cols(find(vlmt == 0));
  }

  inline void pull_points(arma::vec Qe, uvec const &TC, double theta_pull) {
    for (arma::uword iq = 0; iq < size(TC, 0); ++iq) {
      Qe(3 * TC(iq) + 0) = cos(theta_pull);
      Qe(3 * TC(iq) + 1) = 0.0;
      Qe(3 * TC(iq) + 2) = sin(theta_pull);
    }
  }

  inline void compress(arma::vec *Qe, uvec const &TC, arma::mat const &x, double length_step) {
    arma::rowvec maxx = max(x);
    for (arma::uword iq = 0; iq < size(TC, 0); ++iq) {
      if (x.at(TC(iq), 0) < 0.5 * length_step) {
        Qe->at(3 * TC(iq) + 0) = 1;
      } else if (x.at(TC(iq), 0) > maxx(0) - 0.5 * length_step) {
        Qe->at(3 * TC(iq) + 0) = -1;
      }
      if (x.at(TC(iq), 1) < 0.5 * length_step) {
        Qe->at(3 * TC(iq) + 1) = 1;
      } else if (x.at(TC(iq), 1) > maxx(1) - 0.5 * length_step) {
        Qe->at(3 * TC(iq) + 1) = -1;
      }
    }
  }

  //*----------------------------*
  //|       Find functions       |
  //*----------------------------*
  inline uvec findPointAlongY(arma::mat const &x, double xstart, double length_step) {
    arma::rowvec maxx = max(x);

    return find((abs(x.row(0) - xstart) < 0.5 * length_step) &&
                abs(x.row(1) - (maxx(1) / 2)) < 0.5 * length_step);
  }

  inline uvec findDraglinePoints(arma::mat const &x, double cstart, double clength, double length_step) {
    arma::rowvec maxx = max(x);

    return find(x.row(0) > cstart - length_step * 1E-1 &&
                x.row(0) < cstart + clength + length_step * 1E-1 &&
                abs(x.row(1) - (maxx(1) / 2)) < 0.5 * length_step);
  }

  // Finds border (side) of x by using length_step. majmin == true finds the border of maximum
  // cohordinates
  inline uvec findBorder(arma::mat const &x, double length_step, arma::uword side, bool majmin) {
    arma::rowvec maxx = max(x.t());
    if (majmin)
      return find(abs(x.row(side) - maxx(side)) < length_step);
    else
      return find(abs(x.row(side)) < length_step);
  }

  inline uvec findBorders(arma::mat const &x, double length_step) {
    arma::rowvec maxx = arma::max(x);
    return find((abs(x.row(0)) < 0.5 * length_step) || (abs(x.row(1)) < 0.5 * length_step) ||
                (abs(x.row(0) - maxx(0)) < 0.5 * length_step) ||
                (abs(x.row(1) - maxx(1)) < 0.5 * length_step));
  }

  inline uvec findFrontBorder(arma::mat const &x, double length_step) {
    return find(abs(x.row(0)) < 0.5 * length_step);
  }

  inline uvec findCircleAroundPoint(arma::mat const &x, double xp, double yp, double radius,
                                    double length_step) {
    return find(sqrt(pow(x.row(0) - xp, 2) + pow(x.row(1) - yp, 2)) < (0.5 * length_step + radius));
  }

  inline uvec findRectangle(arma::mat const &x, double xs, double xe, double ys, double ye,
                            double length_step) {
    return find(abs(x.row(0)) > xs - 0.5 * length_step && abs(x.row(0)) < xe + 0.5 * length_step &&
                abs(x.row(1)) > ys - 0.5 * length_step && abs(x.row(1)) < ye + 0.5 * length_step);
  }

  inline uvec findPattern(arma::mat const &x, double w, double d_w, double angle, double length_step) {
    uvec F = arma::zeros<uvec>(0);
    for (double np = -5; np < 5; np++) {
      F = join_cols(F, find((x.row(1) - tan(angle) * x.row(0)) - d_w / cos(angle) -
                                    np * (w + d_w) / cos(angle) + 0.4 * length_step >
                                0 &&
                            (x.row(1) - tan(angle) * x.row(0)) - (np + 1) * (w + d_w) / cos(angle) -
                                    0.4 * length_step <
                                0));
      //// abs(x.row(1)  + 1/tan(angle)*x.row(0)) > pl/sin(angle) - 0.4 * length_step &&
      //// abs(x.row(1)  + 1/tan(angle)*x.row(0)) < pl/sin(angle) + pl_d/sin(angle) - 0.4 * length_step
    }
    return F;
  }
}  // namespace armautils

#endif  // DLF_ARMAUTILITIES_HPP_
