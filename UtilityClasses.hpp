/**
 * @file UtilityClasses.hpp
 * @author Daniele Liprandi (daniele.liprandi@gmail.com)
 * @brief
 * @date 2020-05-29
 *
 * @copyright Copyright © 2020. This file is under the CC by-nc-sa 4.0 license
 * Creative Commons License, Attribution-NonCommercial-ShareAlike 4.0 International.
 * The full text of this license may be found here:
 * https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 */

#ifndef DLF_UTILITYCLASSES_HPP_
#define DLF_UTILITYCLASSES_HPP_

#include <ctime>
#include <iostream>
#include <string>

namespace dlf {
/* -------------------------------------------------------------------------- */
/*                           Timer Class Definition                           */
/* -------------------------------------------------------------------------- */
class Timer {
 public:
  /**
   * @brief Construct a new Timer object
   *
   */
  inline Timer() { clock_gettime(CLOCK_REALTIME, &beg_); }

  /**
   * @brief calculates elapsed time
   *
   * @return double number of seconds
   */
  inline double elapsed() {
    clock_gettime(CLOCK_REALTIME, &end_);
    return end_.tv_sec - beg_.tv_sec + (end_.tv_nsec - beg_.tv_nsec) / 1000000000.;
  }
  /**
   * @brief reset clock
   *
   */
  inline void reset() { clock_gettime(CLOCK_REALTIME, &beg_); }
  /**
   * @brief produce a complete timestamp
   *
   * @return std::string timestamp with format YYYY-MM-DD--HH-MM-SS
   */
  inline std::string timestamp() {
    auto now = std::time(nullptr);
    char buf[sizeof("YYYY-MM-DD  HH-MM-SS:")];
    return std::string(buf, buf + std::strftime(buf, sizeof(buf), "%F--%H-%M-%S", std::gmtime(&now)));
  }

 private:
  timespec beg_, ///< clock start
      end_;      ///< clock end
};

/* -------------------------------------------------------------------------- */
/*                         CycleBase Class Definition                         */
/* -------------------------------------------------------------------------- */

class CycleBase {
 public:
  inline void increase_cycle() { stp += 1; }
  bool keep_looping() { return stp < maxcycles; }
  inline uint get_stp() const { return stp; }
  explicit inline CycleBase(uint maxcycles) { this->maxcycles = maxcycles; }

 private:
  uint stp = 0;
  uint maxcycles = 10;
  int cycle_ID = -1;
};

struct CycleFixedIncrement : public CycleBase {
  double increment = 0;
  double current_value = 0;
  explicit inline CycleFixedIncrement(uint maxcycles) : CycleBase(maxcycles) {}
};

struct CycleWithRefinement : public CycleFixedIncrement {
  uint refinements = 0;
  uint consecutive_refinements = 0;
  bool calculation_success = 0;
  explicit inline CycleWithRefinement(uint maxcycles) : CycleFixedIncrement(maxcycles) {}
  inline void modifyloop() { // If it was too hard reduce the displacement step
                             // deps
    // evoke_exit(P, saved_once);

    //? I'm going to delete "u = P.u_result.col(stp);" since I should have fixed it for displacement
    //? control
    increment /= 2;
    refinements++;
    consecutive_refinements++;
    calculation_success = true;
  }
};

struct CycleWithNoChangesControl : public CycleWithRefinement {
  bool increment_too_low = false;
  bool local_kl = true;
  uint no_changes = 0;
  double increment = 0;
  double current_value = 0;

  inline bool action_needed() const { return (increment_too_low); }
  inline void modifyloop() {
    increment *= 2;
    if (refinements != 0)
      refinements--;
    no_changes = 0;
    increment_too_low = false;
    std::cout << "refstrike: " << consecutive_refinements << std::endl;
  }
};
} // namespace dlf
#endif // DLF_UTILITY_CLASSES_HPP_
