/**
 * @file EigenArmaInteraction.hpp
 * @author Daniele Liprandi (daniele.liprandi@gmail.com)
 * @brief
 * @date 2020-05-29
 *
 * @copyright Copyright © 2020. This file is under the CC by-nc-sa 4.0 license
 * Creative Commons License, Attribution-NonCommercial-ShareAlike 4.0 International.
 * The full text of this license may be found here:
 * https://creativecommons.org/licenses/by-nc-sa/4.0/
 *
 */

#ifndef DLF_EIGENARMAINTERACTION_HPP_
#define DLF_EIGENARMAINTERACTION_HPP_

#include <armadillo>

/* -------------------------------------------------------------------------- */
/*                        Eigen-Arma casting and copies                       */
/* -------------------------------------------------------------------------- */

template <typename T_a, typename T_b> inline void deepCopyEigen(T_a const &a, T_b &b) {
  b = T_b(a.n_elem);
  for (uint i = 0; i < a.n_elem; i++) {
    b(i) = a(i);
  }
  return b;
}

template <typename T_a, typename T_b> inline T_b deepCopyEigen(T_a const &a) {
  T_b b = T_b(a.n_elem);
  for (uint i = 0; i < a.n_elem; i++) {
    b(i) = a(i);
  }
  return b;
}

template <typename T_a, typename T_b> inline void deepCopyArma(T_a const &a, T_b &b) {
  b = T_b(a.size());
  for (uint i = 0; i < a.size(); i++) {
    b(i) = a(i);
  }
}

template <typename T_a, typename T_b> inline T_b deepCopyArma(T_a const &a) {
  T_b b = T_b(a.size());
  for (uint i = 0; i < a.size(); i++) {
    b(i) = a(i);
  }
}

// MATRIXES CASTED TO EIGEN BECAME SEPARATE ENTITIES
inline Eigen::Map<Eigen::MatrixXd> cast_eigen_share(arma::mat *arma_A) {
  return Eigen::Map<Eigen::MatrixXd>(arma_A->memptr(), arma_A->n_rows, arma_A->n_cols);
}
inline Eigen::Map<Eigen::MatrixXd> cast_eigen_share(arma::vec *arma_A) {
  return Eigen::Map<Eigen::MatrixXd>(arma_A->memptr(), arma_A->n_rows, arma_A->n_cols);
}

// MATRIXES CASTED TO EIGEN BECAME SEPARATE ENTITIES
inline Eigen::MatrixXd cast_eigen_copy(arma::mat &arma_A) {
  return Eigen::Map<const Eigen::MatrixXd>(arma_A.memptr(), arma_A.n_rows, arma_A.n_cols);
}

// MATRIX CAST TO ARMA SHARES THE SAME MEMORY
inline arma::mat cast_arma_share(Eigen::MatrixXd &eigen_A) {
  arma::mat arma_B = arma::mat(eigen_A.data(), eigen_A.rows(), eigen_A.cols(), false, true);

  return arma_B;
}
inline arma::mat cast_arma_share(Eigen::Array<double, 3, Eigen::Dynamic> &eigen_A) {
  arma::mat arma_B = arma::mat(eigen_A.data(), eigen_A.rows(), eigen_A.cols(), false, true);

  return arma_B;
}
inline arma::Mat<Eigen::Index> cast_arma_share(Eigen::Array<Eigen::Index, 2, Eigen::Dynamic> &eigen_A) {
  arma::Mat<Eigen::Index> arma_B =
      arma::Mat<Eigen::Index>(eigen_A.data(), eigen_A.rows(), eigen_A.cols(), false, true);

  return arma_B;
}

// MATRIX CAST TO ARMA BECOMES SEPARATE ENTITY
inline arma::mat cast_arma_copy(Eigen::MatrixXd &eigen_A) {
  arma::mat arma_B = arma::mat(eigen_A.data(), eigen_A.rows(), eigen_A.cols(), true, false);

  return arma_B;
}

#endif  // DLF_EIGENARMAINTERACTION_HPP_
